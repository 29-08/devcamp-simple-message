//import thu vien express js 
const express = require('express');

//Khoi tao app Express 
const app = express();

//Khai bao cong chay project
const port = 3000;

//Callback function la mot function tham so cura mot function khac, no se thuc hien khi function day duoc goi
//Khai bao api dang /
app.get("/", (req, res) => {
    let today = new Date();

    res.json({
        message:  `Xin chao, hom nay la ngay ${today.getDate()} thang ${today.getMonth()} nam ${today.getFullYear()}`
    })
})

app.listen(port, () => {
    console.log('App listening on port', port);
})